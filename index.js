"use strict"
let allRead = true;

let notifications = [
 { message: "Lorem", read: true },
 { message: "Ipsum", read: true },
 { message: "Dolor", read: true },
 { message: "Sit", read: false },
 { message: "Amet", read: true }
];
// TODO
// 1. Set the allRead variable to false, 
// 2. Using built-in higher-order function, 
// 3. Without using forEach() or any loop

const setAllReadFalse = (accumulator, currentValue) => {
 if (currentValue.read) currentValue.read = !allRead
 accumulator.push(currentValue)
 return accumulator;
}

const res = notifications.reduce(setAllReadFalse, []);

/*
let i = 0;
function execute() {
 
 if (i < notifications.length) {
  if (notifications[i].read) notifications[i].read = !allRead;
  i++;
  execute();
 } else {
  return;
 }
}

execute();
*/

console.log(res); 
